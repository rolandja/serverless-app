const functions = require("firebase-functions")
const admin = require("firebase-admin")

admin.initializeApp()

exports.generateThumbnail = functions.region("europe-west1").storage.object().onFinalize(async (object) => {
	if(object.metadata.resizedImage) {
		let originalURL, thumbURL, originalFileName

		originalFileName = object.name.split("/")[2].split("_")[0]

		await admin.storage().bucket(object.bucket).file("images/" + originalFileName).getSignedUrl({
			action: "read",
			expires: "01-01-2100"
		}).then(url => {
			return originalURL = url[0]
		})

		await admin.storage().bucket(object.bucket).file(object.name).getSignedUrl({
			action: "read",
			expires: "01-01-2100"
		}).then(url => {
			return thumbURL = url[0]
		})

		admin.firestore().collection("gallery").add({
			originalURL: originalURL,
			thumbnailURL: thumbURL,
			timestamp: admin.firestore.FieldValue.serverTimestamp()
		})

		return "It worked"
	} else {
		return "Did not work"
	}
});