# serverless-app

https://serverless-app-5524b.firebaseapp.com/

## Project Info
```
Project to learn more about firebase and serverless architecture
```

## Features

```
Login via gmail / email
Drag and drop images / gifs, etc
Saved into firebase storage
Stored images connected to database
Gallery view of uploaded images
Thumbnails shown (smaller image), on click original image is shown
Restricted access / error handling
Firebase functions used
```

## To run the project

1. Create .env file in the root of the project
2. Copy snippet below in it

```
VUE_APP_FIREBASE_API_KEY = ""
VUE_APP_FIREBASE_AUTH_DOMAIN = ""
VUE_APP_FIREBASE_DATABASE_URL = ""
VUE_APP_FIREBASE_PROJECT_ID = ""
VUE_APP_FIREBASE_STORAGE_BUCKET = ""
VUE_APP_FIREBASE_MESSAGING_SENDER_ID = ""
VUE_APP_FIREBASE_APP_ID = ""
VUE_APP_FIREBASE_MEASUREMENT_ID = ""
```

3. Add your firebase keys


Currently only gmail and email login options are working.