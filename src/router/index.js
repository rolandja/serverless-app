import Vue from 'vue'
import Router from "vue-router"

import Home from "../views/Home.vue"
import Login from "../views/Login.vue"
import Gallery from "../views/Gallery.vue"
import GalleryUpload from "../views/GalleryUpload.vue"
import Profile from "../views/Profile.vue"
import NotFound from "../views/NotFound.vue"

import {isUserAuthenticated} from '../util/firebase'

Vue.use(Router)

let router = new Router({
	mode:'history',
	base: process.env.BASE_URL,
	routes: [
		{
			path: "/",
			name: "Home",
			component: Home
		},
		{
			path: "/login",
			name: "Login",
			component: Login,
			meta: {
				guest: true
			}
		},
		{
			path: "/gallery",
			name: "Gallery",
			component: Gallery,
			meta: {
				authRequired: true
			}
		},
		{
			path: "/gallery/upload",
			name: "GalleryUpload",
			component: GalleryUpload,
			meta: {
				authRequired: true
			}
		},
		{
			path: "/profile",
			name: "Profile",
			component: Profile,
			meta: {
				authRequired: true
			}
		},
		{
			path: "*",
			name: "NotFound",
			component: NotFound
		}
	]
})

router.beforeEach((to, from, next) => {
	if(to.matched.some(record => record.meta.guest)) {
		isUserAuthenticated()
			.then(() => {
				next("/")
			})
			.catch(() => {
				next()
			})
	} else if (to.matched.some(record => record.meta.authRequired)){
		isUserAuthenticated()
			.then(() => {
				next()
			})
			.catch(() => {
				next("/")
			})
	} else {
		next()
	}
})

export default router

