import * as firebase from "firebase"
import * as firebaseui from "firebaseui"

// import "firebase/firestore"
// import "firebase/analytics"
// import "firebase/storage"

export const limitImages = 3	// how many images retrieved at once

// initialize firebase with credentials
const initFirebase = () => {
    // Your web app's Firebase configuration
    const firebaseConfig = {
        apiKey: process.env.VUE_APP_FIREBASE_API_KEY,
        authDomain: process.env.VUE_APP_FIREBASE_AUTH_DOMAIN,
        databaseURL: process.env.VUE_APP_FIREBASE_DATABASE_URL,
        projectId: process.env.VUE_APP_FIREBASE_PROJECT_ID,
        storageBucket: process.env.VUE_APP_FIREBASE_STORAGE_BUCKET,
        messagingSenderId: process.env.VUE_APP_FIREBASE_MESSAGING_SENDER_ID,
        appId: process.env.VUE_APP_FIREBASE_APP_ID,
        measurementId: process.env.VUE_APP_FIREBASE_MEASUREMENT_ID
    };

    // Initialize Firebase
    if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig)
        firebase.analytics()
    }
}

// generates random UUID
const generateUUID = () => {
	return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
		(c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
	)
}

// upload image to firebase storage
export const uploadImages = (file) => {
	initFirebase()
	const storageRef = firebase.storage().ref()

	return storageRef.child("images/" + generateUUID()).put(file)
}

export const retrieveImages = (last) => {
	initFirebase()
	const docRef = firebase.firestore().collection("gallery")
	if(last === null) {
		return docRef.orderBy("timestamp", "desc").limit(limitImages).get()
	} else {
		return docRef.orderBy("timestamp", "desc").startAfter(last).limit(limitImages).get()
	}
}

export const isUserAuthenticated = () => {
    initFirebase()
    return new Promise((resolve, reject) => {
        firebase.auth().onAuthStateChanged((user) => {
            user ? resolve(user) : reject(false)
        })
    })
}

export const logout = () => {
	initFirebase()
	firebase.auth().signOut()
		.then(() => {
			// Sign-out successful.
			console.log("Logout just happened")
			})
		.catch((error) => {
			// An error happened.
			console.log("error just happened: " + error)
		});
}

// use this to authenticate login
export function authenticateUser(email, password)  {
    initFirebase()
    return new Promise((resolve, reject) => {
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then((data) => {
                // success
                resolve(data)
            })
            .catch((error) => {
                // Handle Errors here.
                reject(error)
            })
    })
}

// create new user
export const registerNewUser = (user, password) => {
    initFirebase()
    return new Promise((resolve, reject) => {
        return firebase.auth().createUserWithEmailAndPassword(user, password)
            .then((data) => {
                resolve(data)
            })
            .catch((error) => {
                // Handle Errors here.
                reject(error)
            });
    })
}

// creates buttons for single sign-on (google, facebok, etc) depending what is described
export const firebaseUI = () => {
    initFirebase()
	const ui = firebaseui.auth.AuthUI.getInstance() || new firebaseui.auth.AuthUI(firebase.auth())

    // firebase login ui config
    const firebaseUiConfig = {
        callbacks: {
            signInSuccessWithAuthResult: function(authResult, redirectUrl) {
				console.log(authResult)
				console.log(redirectUrl)
                // User successfully signed in.
                // Return type determines whether we continue the redirect automatically
                // or whether we leave that to developer to handle.
                return true;
            },
            uiShown: function() {
                // The widget is rendered.
                // Hide the loader.
                document.getElementById('loader').style.display = 'none';
            }
        },
        signInFlow: 'popup',
        signInSuccessUrl: process.env.BASE_URL,
        signInOptions: [
			{
				// Leave the lines as is for the providers you want to offer your users.
				provider: firebase.auth.GoogleAuthProvider.PROVIDER_ID,
				customParameters: {
					// Forces password re-entry.
					prompt: 'select_account'
				},
			},
			{
				provider: firebase.auth.FacebookAuthProvider.PROVIDER_ID,
				scopes: [
					"email",
					"public_profile"
				],
				customParameters: {
					auth_type: "reauthenticate"
				}
			},
			{
				provider: firebase.auth.TwitterAuthProvider.PROVIDER_ID
			},
			{
				provider: firebase.auth.EmailAuthProvider.PROVIDER_ID
			}
        ],
        // Terms of service url.
        // tosUrl: '<your-tos-url>',
        // Privacy policy url.
        // privacyPolicyUrl: '<your-privacy-policy-url>'
    }

    // enable firebase login ui in a specific html element
    ui.start('#firebaseui-auth-container', firebaseUiConfig)
}